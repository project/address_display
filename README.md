# Address display

Provides a address display format for displaying address. You can display
address components which you want, change order using tabledrag widget.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/address_display).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/address_display).


## Table of contents

- Requirements
- Installation
- Usage
- Maintainers


## Requirements

This module requires the following modules:

- [Address](https://www.drupal.org/project/address)


## Installation

Install via composer use
`composer require 'drupal/address_display:^1.x'`

Or

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Usage

1. Add your address field to your entity
2. Go to 'Manage display' tab and select 'Address Display' formatter.
3. Select which components you want to display, change order and set yourdelimiter.

 
## Maintainers

- Szczepan Musial - [lamp5](https://www.drupal.org/u/lamp5)
